package com.aldyaz.scanme.source

import android.content.Context
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import com.aldyaz.scanme.exception.UnsupportedTypeException
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject

class ImageRecognizerDataSourceImpl @Inject constructor(
    @ApplicationContext
    private val context: Context,
    private val textRecognizer: FirebaseVisionTextRecognizer
) : ImageRecognizerDataSource {

    companion object {
        private val arithmeticRegex = Regex("[a-zA-Z]+")
    }

    override fun obtainOperation(uri: Uri): Flow<String> = callbackFlow {
        val contentResolver = context.contentResolver
        val bitmap = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
            MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
        } else {
            ImageDecoder.decodeBitmap(
                ImageDecoder.createSource(contentResolver, uri)
            )
        }

        textRecognizer.processImage(
            FirebaseVisionImage.fromBitmap(bitmap)
        ).addOnSuccessListener {
            val text = it.text
            val isContainsAlphabet = text.matches(arithmeticRegex)

            if (isContainsAlphabet) {
                throw UnsupportedTypeException(
                    "Only support arithmetic expression!"
                )
            }

            trySendBlocking(it.text)
        }.addOnFailureListener {
            it.printStackTrace()
            throw it
        }

        awaitClose {
            textRecognizer.close()
        }
    }
}
