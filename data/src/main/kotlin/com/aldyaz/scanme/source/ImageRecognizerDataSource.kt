package com.aldyaz.scanme.source

import android.net.Uri
import kotlinx.coroutines.flow.Flow

interface ImageRecognizerDataSource {

    fun obtainOperation(uri: Uri): Flow<String>

}