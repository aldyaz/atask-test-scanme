package com.aldyaz.scanme.source

import com.aldyaz.scanme.model.HistoryCache
import kotlinx.coroutines.flow.Flow

interface HistoryLocalDataSource {

    suspend fun insertDb(history: HistoryCache)

    fun fetchHistoriesDb(): Flow<List<HistoryCache>>

    suspend fun writeEncryptionHistory(history: HistoryCache)

    fun readEncryptedHistories(): Flow<List<HistoryCache>>

}