package com.aldyaz.scanme.source

import com.aldyaz.scanme.base.extractResponse
import com.aldyaz.scanme.source.remote.MathApi
import javax.inject.Inject

class MathRemoteDataSourceImpl @Inject constructor(
    private val api: MathApi
) : MathRemoteDataSource {

    override suspend fun fetchResult(operation: String): String {
        return api.fetchResult(operation).extractResponse()
    }
}