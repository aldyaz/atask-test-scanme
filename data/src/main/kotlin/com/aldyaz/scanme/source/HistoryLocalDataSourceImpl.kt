package com.aldyaz.scanme.source

import com.aldyaz.scanme.model.HistoryCache
import com.aldyaz.scanme.source.local.HistoryDao
import com.aldyaz.scanme.source.local.HistoryFileManager
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class HistoryLocalDataSourceImpl @Inject constructor(
    private val dao: HistoryDao,
    private val historyFileManager: HistoryFileManager
) : HistoryLocalDataSource {

    override suspend fun insertDb(history: HistoryCache) {
        dao.upsert(history)
    }

    override fun fetchHistoriesDb(): Flow<List<HistoryCache>> {
        return dao.fetchAll()
    }

    override suspend fun writeEncryptionHistory(history: HistoryCache) {
        historyFileManager.writeEncryptionHistory(history)
    }

    override fun readEncryptedHistories(): Flow<List<HistoryCache>> {
        return historyFileManager.readEncryptedHistories()
    }
}