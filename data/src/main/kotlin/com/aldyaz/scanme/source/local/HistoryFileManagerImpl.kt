package com.aldyaz.scanme.source.local

import android.content.Context
import com.aldyaz.scanme.model.HistoryCache
import com.aldyaz.scanme.utils.FileSecureManager
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.File
import java.util.Calendar
import javax.inject.Inject

class HistoryFileManagerImpl @Inject constructor(
    @ApplicationContext
    private val context: Context,
    private val fileSecureManager: FileSecureManager,
    moshi: Moshi
) : HistoryFileManager {

    companion object {

        private const val FILE_SUFFIX = "-encrypted-history.txt"
    }

    private val moshiAdapter: JsonAdapter<HistoryCache> = moshi
        .adapter(
            HistoryCache::class.java
        )

    override suspend fun writeEncryptionHistory(history: HistoryCache) {
        val file = File(
            context.filesDir,
            "${Calendar.getInstance().timeInMillis}${FILE_SUFFIX}"
        )
        val json = moshiAdapter.toJson(history)
        fileSecureManager.writeEncrypted(file, json)
    }

    override fun readEncryptedHistories(): Flow<List<HistoryCache>> = flow {
        val histories: MutableList<HistoryCache> = mutableListOf()
        val directory = context.filesDir
        val files = directory.listFiles { _, name ->
            name.endsWith(FILE_SUFFIX)
        }.orEmpty()
        files.forEach { file ->
            val json = fileSecureManager.readEncrypted(file)
            val history = moshiAdapter.fromJson(json)
            if (history != null) {
                histories.add(history)
            }
        }
        emit(histories)
    }
}
