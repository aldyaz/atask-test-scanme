package com.aldyaz.scanme.source.local

import com.aldyaz.scanme.model.HistoryCache
import kotlinx.coroutines.flow.Flow

interface HistoryFileManager {

    suspend fun writeEncryptionHistory(history: HistoryCache)

    fun readEncryptedHistories(): Flow<List<HistoryCache>>

}
