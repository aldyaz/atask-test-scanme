package com.aldyaz.scanme.source.remote

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MathApi {

    @GET(".")
    suspend fun fetchResult(
        @Query("expr") operation: String
    ): Response<String>

}