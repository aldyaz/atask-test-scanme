package com.aldyaz.scanme.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.aldyaz.scanme.model.HistoryCache
import kotlinx.coroutines.flow.Flow

@Dao
interface HistoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(history: HistoryCache)

    @Transaction
    @Query("SELECT * FROM history")
    fun fetchAll(): Flow<List<HistoryCache>>

}