package com.aldyaz.scanme.source

interface MathRemoteDataSource {

    suspend fun fetchResult(operation: String): String

}