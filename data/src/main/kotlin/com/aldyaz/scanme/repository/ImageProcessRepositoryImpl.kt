package com.aldyaz.scanme.repository

import android.net.Uri
import android.util.Log
import com.aldyaz.scanme.base.DatabaseTransactionRunner
import com.aldyaz.scanme.model.HistoryCache
import com.aldyaz.scanme.model.StorageType
import com.aldyaz.scanme.source.HistoryLocalDataSource
import com.aldyaz.scanme.source.ImageRecognizerDataSource
import com.aldyaz.scanme.source.MathRemoteDataSource
import kotlinx.coroutines.flow.first
import javax.inject.Inject
import kotlin.random.Random

class ImageProcessRepositoryImpl @Inject constructor(
    private val dataSource: ImageRecognizerDataSource,
    private val mathRemoteDataSource: MathRemoteDataSource,
    private val historyLocalDataSource: HistoryLocalDataSource,
    private val transactionRunner: DatabaseTransactionRunner
) : ImageProcessRepository {

    override suspend fun processImage(
        storageType: StorageType,
        imagePath: String
    ) {
        val uri = Uri.parse(imagePath)
        val operation = dataSource.obtainOperation(uri).first().trim()
        val result = mathRemoteDataSource.fetchResult(operation)
        transactionRunner {
            val history = HistoryCache(
                operation = operation,
                result = result
            )
            when(storageType) {
                StorageType.FILE -> {
                    historyLocalDataSource.writeEncryptionHistory(
                        history.copy(
                            id = Random.nextInt()
                        )
                    )
                }
                StorageType.DATABASE -> {
                    historyLocalDataSource.insertDb(history)
                }
            }
        }
    }
}
