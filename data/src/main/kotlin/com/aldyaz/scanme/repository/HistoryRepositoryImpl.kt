package com.aldyaz.scanme.repository

import com.aldyaz.scanme.model.History
import com.aldyaz.scanme.source.HistoryLocalDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

class HistoryRepositoryImpl @Inject constructor(
    private val localDataSource: HistoryLocalDataSource
) : HistoryRepository {

    override fun fetchHistories(): Flow<List<History>> {
        return combine(
            localDataSource.fetchHistoriesDb(),
            localDataSource.readEncryptedHistories()
        ) { dbHistories, fileHistories ->
            (dbHistories + fileHistories)
                .sortedBy { item -> item.timestamp }
                .map { history ->
                    History(
                        operation = history.operation,
                        result = history.result
                    )
                }
        }
    }
}