package com.aldyaz.scanme.exception

import java.io.IOException

class CloudApiException(
    override val message: String = "Api Error!"
) : IOException()