package com.aldyaz.scanme.exception

import java.io.IOException

class UnsupportedTypeException(
    override val message: String = "Type is not supported!"
) : IOException(message)