package com.aldyaz.scanme.di

import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class FirebaseModule {

    @Singleton
    @Provides
    fun provideFirebaseVision(): FirebaseVision = FirebaseVision.getInstance()

    @Singleton
    @Provides
    fun provideFirebaseTextRecognizer(
        firebaseVision: FirebaseVision
    ): FirebaseVisionTextRecognizer = firebaseVision.onDeviceTextRecognizer

}