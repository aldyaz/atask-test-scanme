package com.aldyaz.scanme.di

import com.aldyaz.scanme.repository.HistoryRepository
import com.aldyaz.scanme.repository.HistoryRepositoryImpl
import com.aldyaz.scanme.repository.ImageProcessRepository
import com.aldyaz.scanme.repository.ImageProcessRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class RepositoryModule {

    @Binds
    abstract fun bindImageProcessRepository(
        impl: ImageProcessRepositoryImpl
    ): ImageProcessRepository

    @Binds
    abstract fun bindHistoryRepository(
        impl: HistoryRepositoryImpl
    ): HistoryRepository

}