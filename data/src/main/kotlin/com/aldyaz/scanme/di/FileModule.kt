package com.aldyaz.scanme.di

import com.aldyaz.scanme.source.local.HistoryFileManager
import com.aldyaz.scanme.source.local.HistoryFileManagerImpl
import com.aldyaz.scanme.utils.FileSecureManager
import com.aldyaz.scanme.utils.FileSecureManagerImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class FileModule {

    @Binds
    abstract fun bindFileSecureManager(
        impl: FileSecureManagerImpl
    ): FileSecureManager

    @Binds
    abstract fun bindHistoryFileManager(
        impl: HistoryFileManagerImpl
    ): HistoryFileManager

}