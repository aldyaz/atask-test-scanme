package com.aldyaz.scanme.di

import com.aldyaz.scanme.source.HistoryLocalDataSource
import com.aldyaz.scanme.source.HistoryLocalDataSourceImpl
import com.aldyaz.scanme.source.ImageRecognizerDataSource
import com.aldyaz.scanme.source.ImageRecognizerDataSourceImpl
import com.aldyaz.scanme.source.MathRemoteDataSource
import com.aldyaz.scanme.source.MathRemoteDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class DataSourceModule {

    @Binds
    abstract fun bindImageRecognizerDataSource(
        impl: ImageRecognizerDataSourceImpl
    ): ImageRecognizerDataSource

    @Binds
    abstract fun bindHistoryLocalDataSource(
        impl: HistoryLocalDataSourceImpl
    ): HistoryLocalDataSource

    @Binds
    abstract fun bindMathRemoteDataSource(
        impl: MathRemoteDataSourceImpl
    ): MathRemoteDataSource

}