package com.aldyaz.scanme.di

import android.content.Context
import com.aldyaz.scanme.base.DatabaseTransactionRunner
import com.aldyaz.scanme.base.RoomTransactionRunner
import com.aldyaz.scanme.base.ScanMeDatabase
import com.aldyaz.scanme.source.local.HistoryDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context
    ): ScanMeDatabase = ScanMeDatabase.instance(context)

    @Provides
    fun provideHistoryDao(
        database: ScanMeDatabase
    ): HistoryDao = database.historyDao()

    @Provides
    fun provideRoomTransactionRunner(
        database: ScanMeDatabase
    ): DatabaseTransactionRunner = RoomTransactionRunner(database)

}