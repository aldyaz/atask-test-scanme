package com.aldyaz.scanme.base

import androidx.room.withTransaction

class RoomTransactionRunner(
    private val database: ScanMeDatabase
) : DatabaseTransactionRunner {

    override suspend fun <T> invoke(func: suspend () -> T): T {
        return database.withTransaction(func)
    }
}