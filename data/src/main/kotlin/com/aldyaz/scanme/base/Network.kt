package com.aldyaz.scanme.base

import com.aldyaz.scanme.exception.CloudApiException
import retrofit2.Response

fun <T> Response<T>.extractResponse(): T {
    val body = body()
    if (isSuccessful && body != null) {
        return body
    } else {
        throw CloudApiException()
    }
}