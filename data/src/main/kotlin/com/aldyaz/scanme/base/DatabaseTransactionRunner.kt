package com.aldyaz.scanme.base

interface DatabaseTransactionRunner {

    suspend operator fun <T> invoke(func: suspend () -> T): T

}