package com.aldyaz.scanme.base

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.aldyaz.scanme.model.HistoryCache
import com.aldyaz.scanme.source.local.HistoryDao

@Database(
    entities = [
        HistoryCache::class
    ],
    version = ScanMeDatabase.DB_VERSION
)
abstract class ScanMeDatabase : RoomDatabase() {

    companion object {

        private const val DB_NAME = "movix_db"
        const val DB_VERSION = 1

        fun instance(context: Context): ScanMeDatabase {
            val database = Room.databaseBuilder(context, ScanMeDatabase::class.java, DB_NAME)
            return database
                .allowMainThreadQueries()
                .build()
        }
    }

    abstract fun historyDao(): HistoryDao

}