package com.aldyaz.scanme.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HistoryCacheWrapper(
    val histories: List<HistoryCache>
)