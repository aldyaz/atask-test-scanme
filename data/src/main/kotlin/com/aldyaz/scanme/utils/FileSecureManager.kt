package com.aldyaz.scanme.utils

import java.io.File

interface FileSecureManager {

    fun writeEncrypted(file: File, content: String)

    fun readEncrypted(file: File): String

}