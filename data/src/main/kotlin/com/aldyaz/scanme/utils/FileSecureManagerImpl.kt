package com.aldyaz.scanme.utils

import android.content.Context
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.MasterKeys
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.File
import java.nio.charset.StandardCharsets
import javax.inject.Inject

class FileSecureManagerImpl @Inject constructor(
    @ApplicationContext
    private val context: Context
) : FileSecureManager {

    private val masterKeyAlias by lazy {
        MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
    }

    private var encryptedFile: EncryptedFile? = null

    override fun writeEncrypted(file: File, content: String) {
        encryptedFile = createEncryptedFile(file)
        encryptedFile?.openFileOutput()?.use {
            it.write(content.toByteArray(StandardCharsets.UTF_8))
            it.flush()
        }
    }

    override fun readEncrypted(file: File): String {
        val fileReadBytes = file.readBytes()
        var numBytesRead = 0
        encryptedFile = createEncryptedFile(file)
        encryptedFile?.openFileInput()?.use {
            numBytesRead = it.read(fileReadBytes)
        }
        return String(fileReadBytes, 0, numBytesRead)
    }

    private fun createEncryptedFile(file: File): EncryptedFile {
        return EncryptedFile.Builder(
            file,
            context,
            masterKeyAlias,
            EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build()
    }

}