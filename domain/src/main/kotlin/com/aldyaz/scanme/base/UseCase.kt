package com.aldyaz.scanme.base

abstract class UseCase<PARAM, RESULT : Any> {

    protected abstract suspend fun build(param: PARAM): RESULT

    suspend operator fun invoke(param: PARAM): RESULT = build(param)

}