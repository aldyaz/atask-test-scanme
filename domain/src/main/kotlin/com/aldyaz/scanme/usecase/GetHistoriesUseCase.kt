package com.aldyaz.scanme.usecase

import com.aldyaz.scanme.base.UseCase
import com.aldyaz.scanme.model.History
import com.aldyaz.scanme.repository.HistoryRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetHistoriesUseCase @Inject constructor(
    private val repository: HistoryRepository
) : UseCase<Unit, Flow<List<History>>>() {

    override suspend fun build(param: Unit): Flow<List<History>> {
        return repository.fetchHistories()
    }
}
