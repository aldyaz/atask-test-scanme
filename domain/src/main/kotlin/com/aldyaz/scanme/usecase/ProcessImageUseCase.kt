package com.aldyaz.scanme.usecase

import com.aldyaz.scanme.model.StorageType
import com.aldyaz.scanme.repository.ImageProcessRepository
import javax.inject.Inject

class ProcessImageUseCase @Inject constructor(
    private val repository: ImageProcessRepository
) {

    suspend operator fun invoke(storageType: StorageType, imagePath: String) {
        return repository.processImage(storageType, imagePath)
    }
}
