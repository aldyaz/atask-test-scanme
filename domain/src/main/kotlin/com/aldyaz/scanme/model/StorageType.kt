package com.aldyaz.scanme.model

enum class StorageType {
    FILE,
    DATABASE
}