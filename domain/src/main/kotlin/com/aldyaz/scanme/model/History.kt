package com.aldyaz.scanme.model

data class History(
    val operation: String,
    val result: String
)