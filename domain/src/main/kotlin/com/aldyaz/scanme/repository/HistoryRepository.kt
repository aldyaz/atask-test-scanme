package com.aldyaz.scanme.repository

import com.aldyaz.scanme.model.History
import kotlinx.coroutines.flow.Flow

interface HistoryRepository {

    fun fetchHistories(): Flow<List<History>>

}