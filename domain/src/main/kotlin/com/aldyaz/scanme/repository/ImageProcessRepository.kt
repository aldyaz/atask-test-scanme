package com.aldyaz.scanme.repository

import com.aldyaz.scanme.model.StorageType

interface ImageProcessRepository {

    suspend fun processImage(storageType: StorageType, imagePath: String)

}