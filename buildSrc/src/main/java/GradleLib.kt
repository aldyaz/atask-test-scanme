object GradleLib {

    private const val pluginVersion = "7.2.1"

    const val classpath = "com.android.tools.build:gradle:$pluginVersion"

}