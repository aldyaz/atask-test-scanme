object AndroidX {

    private const val coreVersion = "1.10.1"
    private const val annotationVersion = "1.6.0"
    private const val browserVersion = "1.5.0"
    private const val hiltCompilerVersion = "1.0.0"
    private const val cameraVersion = "1.2.3"
    private const val securityCryptoVersion = "1.0.0"

    const val archVersion = "2.1.0"

    const val core = "androidx.core:core-ktx:$coreVersion"
    const val annotationLib = "androidx.annotation:annotation:$annotationVersion"
    const val browser = "androidx.browser:browser:$browserVersion"
    const val hiltCompiler = "androidx.hilt:hilt-compiler:$hiltCompilerVersion"

    const val securityCrypto = "androidx.security:security-crypto:$securityCryptoVersion"

}