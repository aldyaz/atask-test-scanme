object TestLib {

    private const val junitVersion = "4.12"
    private const val coreVersion = "1.5.0"
    private const val mockitoKotlinVersion = "2.2.0"
    private const val mockitoVersion = "5.3.1"
    private const val mockkVersion = "4.1.0"

    const val junit = "junit:junit:$junitVersion"
    const val core = "androidx.test:core:$coreVersion"
    const val junitKotlin = "org.jetbrains.kotlin:kotlin-test-junit:${KotlinLib.version}"
    const val mockito = "org.mockito:mockito-core:$mockitoVersion"
    const val mockitoAndrod = "org.mockito:mockito-android:$mockitoVersion"
    const val mockitoKotlin = "org.mockito.kotlin:mockito-kotlin:$mockitoKotlinVersion"
    const val mockk = "io.mockk:mockk:$mockkVersion"

}