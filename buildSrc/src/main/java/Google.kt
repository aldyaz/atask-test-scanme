object Google {

    private const val materialVersion = "1.4.0"
    private const val daggerVersion = "2.42"
    private const val hiltVersion = "2.44"
    private const val mlVisionVersion = "24.0.3"
    private const val servicesVersion = "4.3.15"
    private const val firebaseBomVersion = "32.0.0"

    const val material = "com.google.android.material:material:$materialVersion"

    const val daggerCore = "com.google.dagger:dagger:$daggerVersion"
    const val daggerAndroid = "com.google.dagger:dagger-android:$daggerVersion"
    const val daggerAndroidSupport = "com.google.dagger:dagger-android-support:$daggerVersion"
    const val daggerProcessor = "com.google.dagger:dagger-android-processor:$daggerVersion"
    const val daggerCompiler = "com.google.dagger:dagger-compiler:$daggerVersion"

    const val hiltClasspath = "com.google.dagger:hilt-android-gradle-plugin:$hiltVersion"
    const val hiltAndroid = "com.google.dagger:hilt-android:$hiltVersion"
    const val hiltCompiler = "com.google.dagger:hilt-android-compiler:$hiltVersion"

    const val servicesClasspath = "com.google.gms:google-services:$servicesVersion"

    const val firebaseBom = "com.google.firebase:firebase-bom:$firebaseBomVersion"
    const val firebaseMlVision = "com.google.firebase:firebase-ml-vision:$mlVisionVersion"

}