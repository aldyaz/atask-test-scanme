object AndroidLifecycle {

    private const val version = "2.6.1"

    const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:$version"
    const val liveData = "androidx.lifecycle:lifecycle-livedata-ktx:$version"
    const val compiler = "androidx.lifecycle:lifecycle-compiler:$version"

}