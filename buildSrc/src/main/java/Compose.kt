object Compose {

    private const val animationVersion = "1.4.3"
    private const val uiVersion = "1.4.3"
    private const val foundationVersion = "1.4.3"
    private const val materialVersion = "1.4.3"
    private const val lifecycleViewModelVersion = "2.6.1"
    private const val runtimeVersion = "1.4.3"
    private const val navigationComposeVersion = "2.5.3"
    private const val hiltNavigationComposeVersion = "1.0.0"
    private const val activityComposeVersion = "1.4.0"
    private const val constraintLayoutVersion = "1.0.1"
    private const val toolingVersion = "1.4.2"
    private const val lifecycleVersion = "1.4.0"

    const val compilerVersion = "1.4.7"

    const val animation = "androidx.compose.animation:animation:$animationVersion"
    const val foundation = "androidx.compose.foundation:foundation:$foundationVersion"
    const val material = "androidx.compose.material:material:$materialVersion"

    const val runtime = "androidx.compose.runtime:runtime:$runtimeVersion"
    const val runtimeLiveData = "androidx.compose.runtime:runtime-livedata:$runtimeVersion"

    const val ui = "androidx.compose.ui:ui:$uiVersion"
    const val tooling = "androidx.compose.ui:ui-tooling-preview:$toolingVersion"

    const val lifecycleViewModel =
        "androidx.lifecycle:lifecycle-viewmodel-compose:$lifecycleViewModelVersion"

    const val navigation = "androidx.navigation:navigation-compose:$navigationComposeVersion"
    const val hiltNavigation = "androidx.hilt:hilt-navigation-compose:$hiltNavigationComposeVersion"

    const val activityCompose = "androidx.activity:activity-compose:$activityComposeVersion"

    const val constraintLayout = "androidx.constraintlayout:constraintlayout-compose:$constraintLayoutVersion"

    const val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime-compose:$lifecycleVersion"

}