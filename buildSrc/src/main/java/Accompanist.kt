object Accompanist {

    private const val permissionVersion = "0.30.1"

    const val permission = "com.google.accompanist:accompanist-permissions:$permissionVersion"

}