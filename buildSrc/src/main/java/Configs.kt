object Configs {

    const val compileSdkVersion = 33
    const val minSdkVersion = 23
    const val targetSdkVersion = 33
    const val buildToolsVersion = "30.0.3"

}