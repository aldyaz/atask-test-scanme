# ScanMe!
An app which can perform camera scanning to capture arithmatic expression from built-in camera.

## Stuff Included
- Clean Architecture Project Structure
- Android Architecture Components
- Jetpack Compose
- Hilt x Dagger 2
- Kotlin Coroutines & Flow
- Retrofit & OkHttp
- Moshi
- Room
- Firebase ML Text Recognizer
- MathJS API for calculation needs
- AndroidX Security Crypto for encryption/decryption