package com.aldyaz.scanme.component

import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable

@Composable
fun MainAppBar(
    title: String
) {
    TopAppBar(
        title = {
            Text(text = title)
        }
    )
}