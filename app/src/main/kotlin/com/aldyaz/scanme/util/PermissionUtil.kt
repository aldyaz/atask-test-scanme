@file:OptIn(ExperimentalPermissionsApi::class)

package com.aldyaz.scanme.util

import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.shouldShowRationale

fun handleRequestPermission(
    permissionState: PermissionState,
    onGranted: () -> Unit,
    onShouldShowRationale: () -> Unit,
    onDenied: () -> Unit
) {
    if (permissionState.status.isGranted) {
        onGranted()
    } else {
        if (permissionState.status.shouldShowRationale) {
            onShouldShowRationale()
        } else {
            onDenied()
        }
    }
}