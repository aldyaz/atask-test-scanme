package com.aldyaz.scanme.util

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import androidx.core.content.FileProvider
import com.aldyaz.scanme.BuildConfig
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.lang.ref.WeakReference

object BitmapUtils {

    fun toUri(context: Context, bitmap: Bitmap): Uri? {
        val result: WeakReference<Bitmap> = WeakReference(
            Bitmap.createScaledBitmap(bitmap, bitmap.width, bitmap.height, false).copy(
                Bitmap.Config.RGB_565,
                true
            )
        )
        return saveAndConvertToUri(context, result.get() ?: bitmap)
    }

    private fun saveAndConvertToUri(
        context: Context,
        bitmap: Bitmap
    ): Uri? {
        val imageFolder = File(context.cacheDir, "Pictures")
        return try {
            imageFolder.mkdirs()
            val imageFile = File(imageFolder, "scan_me_image.jpg")
            val fileOS = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOS)
            fileOS.flush()
            fileOS.close()
            FileProvider.getUriForFile(
                context.applicationContext,
                BuildConfig.APPLICATION_ID.plus(".fileprovider"),
                imageFile
            )
        } catch (err: FileNotFoundException) {
            err.printStackTrace()
            null
        } catch (err: IOException) {
            err.printStackTrace()
            null
        }
    }
}