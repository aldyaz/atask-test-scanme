package com.aldyaz.scanme.common

sealed class UiState<out DATA> {

    object Loading : UiState<Nothing>()

    class Error(
        throwable: Throwable
    ) : UiState<Nothing>()

    class Success<DATA>(
        val data: DATA
    ) : UiState<DATA>()

}