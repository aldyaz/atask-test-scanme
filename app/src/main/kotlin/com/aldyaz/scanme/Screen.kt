package com.aldyaz.scanme

sealed class Screen(
    val route: String
) {

    companion object {

        private const val MAIN = "main"
    }

    object Main : Screen(MAIN)

}