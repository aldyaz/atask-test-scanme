package com.aldyaz.scanme.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import com.aldyaz.scanme.color.ColorPalette

private val lightColors = lightColors(
    primary = ColorPalette.Primary
)

@Composable
fun AppTheme(
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colors = lightColors,
        content = content
    )
}