package com.aldyaz.scanme

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.aldyaz.scanme.main.MainScreen
import com.aldyaz.scanme.theme.AppTheme

@Composable
fun ScanMeApp() {
    AppTheme {
        AppNavigation()
    }
}

@Composable
fun AppNavigation() {
    val navHostController = rememberNavController()
    NavHost(
        navController = navHostController,
        startDestination = Screen.Main.route
    ) {
        composable(Screen.Main.route) {
            MainScreen()
        }
    }
}