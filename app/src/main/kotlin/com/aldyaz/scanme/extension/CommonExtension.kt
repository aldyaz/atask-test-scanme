package com.aldyaz.scanme.extension

val <T> T.exhaustive: T
    get() = this