package com.aldyaz.scanme.main.entity

data class ProcessImageState(
    val isLoading: Boolean = false,
    val errorMessage: String = ""
) {

    fun loading() = copy(
        isLoading = true
    )

    fun error(message: String) = copy(
        isLoading = false,
        errorMessage = message
    )

    fun success() = copy(
        isLoading = false,
        errorMessage = ""
    )
}
