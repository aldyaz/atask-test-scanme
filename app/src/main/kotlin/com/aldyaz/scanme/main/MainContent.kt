@file:OptIn(ExperimentalPermissionsApi::class)

package com.aldyaz.scanme.main

import android.widget.Toast
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.aldyaz.scanme.common.UiState
import com.aldyaz.scanme.main.component.HistoryComponent
import com.aldyaz.scanme.main.component.InputButton
import com.aldyaz.scanme.main.component.StorageOptionsRadioGroup
import com.aldyaz.scanme.main.component.rememberPictureInputState
import com.aldyaz.scanme.main.entity.HistoryViewEntity
import com.aldyaz.scanme.main.entity.ProcessImageState
import com.aldyaz.scanme.main.entity.StorageOptions
import com.google.accompanist.permissions.ExperimentalPermissionsApi

@Composable
fun MainContent(
    modifier: Modifier = Modifier,
    historiesState: UiState<List<HistoryViewEntity>>,
    processImageState: ProcessImageState,
    onRetryHistories: () -> Unit,
    onProcessingImage: (String) -> Unit,
    selectedStorageOptionState: MutableState<StorageOptions>
) {

    val context = LocalContext.current

    val pictureInputState = rememberPictureInputState(
        onResult = { uri ->
            onProcessingImage(uri.toString())
        },
        onPermissionDenied = {
            Toast.makeText(context, "Permission Denied!", Toast.LENGTH_SHORT).show()
        }
    )

    val errorMessage = processImageState.errorMessage

    if (processImageState.isLoading) {
        Dialog(
            onDismissRequest = {},
            properties = DialogProperties(
                dismissOnBackPress = false,
                dismissOnClickOutside = false
            ),
            content = {
                Box(
                    modifier = Modifier.fillMaxSize()
                ) {
                    CircularProgressIndicator(
                        modifier = Modifier.align(Alignment.Center)
                    )
                }
            }
        )
    }

    LaunchedEffect(errorMessage) {
        if (errorMessage.isNotEmpty()) {
            Toast.makeText(
                context,
                processImageState.errorMessage,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    ConstraintLayout(modifier = modifier) {
        val (historyListRef, bottomLayoutRef) = createRefs()

        HistoryComponent(
            modifier = Modifier
                .constrainAs(historyListRef) {
                    linkTo(
                        top = parent.top,
                        bottom = bottomLayoutRef.top,
                        bias = 0f
                    )
                    centerHorizontallyTo(parent)
                    width = Dimension.fillToConstraints
                    height = Dimension.fillToConstraints
                },
            state = historiesState,
            onFetchHistories = onRetryHistories
        )

        Card(
            modifier = Modifier
                .constrainAs(bottomLayoutRef) {
                    bottom.linkTo(parent.bottom)
                    centerHorizontallyTo(parent)
                    width = Dimension.fillToConstraints
                },
            backgroundColor = Color.LightGray,
            shape = RoundedCornerShape(
                topStartPercent = 10,
                topEndPercent = 10
            )
        ) {
            ConstraintLayout {
                val (radioRef, buttonRef) = createRefs()

                StorageOptionsRadioGroup(
                    modifier = Modifier
                        .padding(
                            top = 16.dp,
                            start = 16.dp,
                            end = 16.dp
                        )
                        .constrainAs(radioRef) {
                            centerHorizontallyTo(parent, bias = 0f)
                            linkTo(
                                top = parent.top,
                                bottom = buttonRef.top,
                                bias = 0f
                            )
                        },
                    selectedStorageOptionState = selectedStorageOptionState
                )

                InputButton(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp)
                        .constrainAs(buttonRef) {
                            centerHorizontallyTo(parent)
                            linkTo(
                                top = radioRef.bottom,
                                bottom = parent.bottom,
                                bias = 0f
                            )
                        },
                    onClick = {
                        pictureInputState.launchPermissionRequest()
                    }
                )
            }
        }
    }
}
