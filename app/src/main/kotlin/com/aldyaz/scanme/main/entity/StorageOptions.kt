package com.aldyaz.scanme.main.entity

import com.aldyaz.scanme.model.StorageType

enum class StorageOptions(
    val text: String
) {
    FILE("File"),
    DATABASE("Database");

    override fun toString(): String {
        return super.toString().lowercase()
    }

    fun toStorageType(): StorageType {
        return when (this) {
            FILE -> StorageType.FILE
            DATABASE -> StorageType.DATABASE
        }
    }
}