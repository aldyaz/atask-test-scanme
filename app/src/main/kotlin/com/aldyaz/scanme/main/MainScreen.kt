package com.aldyaz.scanme.main

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.aldyaz.scanme.R
import com.aldyaz.scanme.common.UiState
import com.aldyaz.scanme.component.MainAppBar
import com.aldyaz.scanme.main.entity.HistoryViewEntity
import com.aldyaz.scanme.main.entity.ProcessImageState
import com.aldyaz.scanme.main.entity.StorageOptions

@Composable
fun MainScreen() {

    val viewModel: MainViewModel = hiltViewModel()
    val historiesState: UiState<List<HistoryViewEntity>> by viewModel.state.collectAsStateWithLifecycle()
    val processImageState: ProcessImageState by viewModel.processImage.collectAsStateWithLifecycle()
    val selectedStorageOption = remember {
        mutableStateOf(StorageOptions.FILE)
    }

    MainScaffold(
        historiesState = historiesState,
        processImageState = processImageState,
        onFetchHistories = viewModel::getHistories,
        onProcessingImage = {
            viewModel.processImage(selectedStorageOption.value, it)
        },
        selectedStorageOptionState = selectedStorageOption,
    )
}

@Composable
fun MainScaffold(
    historiesState: UiState<List<HistoryViewEntity>>,
    processImageState: ProcessImageState,
    onFetchHistories: () -> Unit,
    onProcessingImage: (String) -> Unit,
    selectedStorageOptionState: MutableState<StorageOptions>
) {
    Scaffold(
        topBar = {
            MainAppBar(title = stringResource(R.string.app_name))
        },
        bottomBar = {},
        content = { innerPadding ->
            MainContent(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(innerPadding),
                historiesState = historiesState,
                processImageState = processImageState,
                onRetryHistories = onFetchHistories,
                onProcessingImage = onProcessingImage,
                selectedStorageOptionState = selectedStorageOptionState
            )
        }
    )
}
