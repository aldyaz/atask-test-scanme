package com.aldyaz.scanme.main.component

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.aldyaz.scanme.R

@Composable
fun InputButton(
    modifier: Modifier = Modifier,
    onClick: () -> Unit
) {
    Surface(
        modifier = modifier
            .clickable(
                onClick = onClick,
                role = Role.Button
            )
            .clip(RectangleShape),
        color = MaterialTheme.colors.primary,
        content = {
            Box {
                Text(
                    text = stringResource(id = R.string.label_bottom_button).uppercase(),
                    modifier = Modifier
                        .padding(16.dp)
                        .align(Alignment.Center),
                    style = MaterialTheme.typography.button.copy(
                        fontWeight = FontWeight.Bold
                    )
                )
            }
        }
    )
}