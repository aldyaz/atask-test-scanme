package com.aldyaz.scanme.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aldyaz.scanme.common.UiState
import com.aldyaz.scanme.main.entity.HistoryViewEntity
import com.aldyaz.scanme.main.entity.ProcessImageState
import com.aldyaz.scanme.main.entity.StorageOptions
import com.aldyaz.scanme.usecase.GetHistoriesUseCase
import com.aldyaz.scanme.usecase.ProcessImageUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val processImageUseCase: ProcessImageUseCase,
    private val getHistoriesUseCase: GetHistoriesUseCase
) : ViewModel() {

    private val _state: MutableStateFlow<UiState<List<HistoryViewEntity>>> = MutableStateFlow(
        UiState.Loading
    )
    val state: StateFlow<UiState<List<HistoryViewEntity>>>
        get() = _state.asStateFlow()

    private val _processImage: MutableStateFlow<ProcessImageState> = MutableStateFlow(
        ProcessImageState()
    )

    val processImage: StateFlow<ProcessImageState>
        get() = _processImage.asStateFlow()

    init {
        getHistories()
    }

    fun getHistories() = viewModelScope.launch(Dispatchers.IO) {
        getHistoriesUseCase(Unit)
            .onStart {
                _state.value = UiState.Loading
            }
            .catch {
                it.printStackTrace()
                _state.value = UiState.Error(it)
            }
            .collect {
                _state.value = UiState.Success(
                    it.map { item ->
                        HistoryViewEntity(
                            operation = item.operation,
                            result = item.result
                        )
                    }
                )
            }
    }

    fun processImage(
        storageOptions: StorageOptions,
        imagePath: String
    ) = viewModelScope.launch {
        if (imagePath.isNotEmpty()) {
            _processImage.update {
                it.loading()
            }
            try {
                processImageUseCase(storageOptions.toStorageType(), imagePath)
                _processImage.update {
                    it.success()
                }
            } catch (err: Throwable) {
                _processImage.update {
                    it.error(err.message.orEmpty())
                }
            }
        }
    }
}
