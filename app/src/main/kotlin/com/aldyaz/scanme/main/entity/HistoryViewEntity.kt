package com.aldyaz.scanme.main.entity

data class HistoryViewEntity(
    val operation: String,
    val result: String
)