package com.aldyaz.scanme.main.component

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.aldyaz.scanme.common.UiState
import com.aldyaz.scanme.component.FullError
import com.aldyaz.scanme.component.FullLoading
import com.aldyaz.scanme.main.entity.HistoryViewEntity

@Composable
fun HistoryComponent(
    modifier: Modifier = Modifier,
    state: UiState<List<HistoryViewEntity>>,
    onFetchHistories: () -> Unit
) {
    when (state) {
        is UiState.Loading -> {
            FullLoading()
        }

        is UiState.Error -> {
            FullError(
                modifier = modifier,
                onRetryClick = onFetchHistories
            )
        }

        is UiState.Success -> {
            HistoryList(
                modifier = modifier,
                histories = state.data
            )
        }
    }
}

@Composable
fun HistoryList(
    modifier: Modifier = Modifier,
    histories: List<HistoryViewEntity>
) {
    LazyColumn(
        modifier = modifier,
        contentPadding = PaddingValues(8.dp),
        verticalArrangement = Arrangement.spacedBy(4.dp)
    ) {
        items(histories) { item ->
            HistoryItem(item)
        }
    }
}

@Composable
fun HistoryItem(history: HistoryViewEntity) {
    Card(
        modifier = Modifier.fillMaxWidth(),
        shape = RoundedCornerShape(4.dp),
        backgroundColor = MaterialTheme.colors.primary
    ) {
        ConstraintLayout {
            val (operationRef, resultRef) = createRefs()
            Text(
                text = history.operation,
                modifier = Modifier
                    .padding(
                        top = 12.dp,
                        start = 12.dp,
                        bottom = 4.dp
                    )
                    .constrainAs(operationRef) {
                        linkTo(parent.top, resultRef.bottom, bias = 0.0f)
                        centerHorizontallyTo(
                            other = parent,
                            bias = 0.0f
                        )
                    },
                color = Color.White
            )
            Text(
                text = history.result,
                modifier = Modifier
                    .padding(
                        top = 4.dp,
                        start = 12.dp,
                        bottom = 12.dp
                    )
                    .constrainAs(resultRef) {
                        linkTo(operationRef.bottom, parent.bottom)
                        centerHorizontallyTo(
                            other = operationRef,
                            bias = 0.0f
                        )
                    },
                color = Color.White
            )
        }
    }
}