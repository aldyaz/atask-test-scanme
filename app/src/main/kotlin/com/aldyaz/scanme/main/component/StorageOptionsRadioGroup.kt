package com.aldyaz.scanme.main.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.MaterialTheme
import androidx.compose.material.RadioButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.aldyaz.scanme.main.entity.StorageOptions

@Composable
fun StorageOptionsRadioGroup(
    modifier: Modifier = Modifier,
    selectedStorageOptionState: MutableState<StorageOptions>
) {
    val items = StorageOptions.values().toList()
    val (selectedStorage, onSelectStorage) = remember {
        mutableStateOf(items[0])
    }

    selectedStorageOptionState.value = selectedStorage

    Column(
        modifier = modifier,
    ) {
        items.forEach { option ->
            Row(
                modifier = Modifier
                    .padding(vertical = 4.dp)
                    .selectable(
                        selected = selectedStorage == option,
                        onClick = {
                            onSelectStorage(option)
                        }
                    ),
                verticalAlignment = Alignment.CenterVertically
            ) {
                RadioButton(
                    selected = selectedStorage == option,
                    onClick = null
                )
                Text(
                    text = option.text,
                    modifier = Modifier.padding(horizontal = 4.dp),
                    style = MaterialTheme.typography.body1
                )
            }
        }
    }
}