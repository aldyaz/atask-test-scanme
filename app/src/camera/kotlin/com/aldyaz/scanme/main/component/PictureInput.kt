@file:OptIn(ExperimentalPermissionsApi::class)

package com.aldyaz.scanme.main.component

import android.Manifest
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.platform.LocalContext
import com.aldyaz.scanme.util.BitmapUtils
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.rememberPermissionState
import kotlinx.coroutines.launch

@Composable
fun rememberPictureInputState(
    onResult: (Uri) -> Unit,
    onPermissionDenied: () -> Unit
): PermissionState = rememberCameraPermissionState(
    onResult = onResult,
    onPermissionDenied = onPermissionDenied
)

@Composable
private fun rememberCameraPermissionState(
    onResult: (Uri) -> Unit,
    onPermissionDenied: () -> Unit
): PermissionState {
    val context = LocalContext.current
    val mainCoroutineScope = rememberCoroutineScope()
    val cameraLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.TakePicturePreview(),
        onResult = { bitmap ->
            mainCoroutineScope.launch {
                if (bitmap != null) {
                    val uri = BitmapUtils.toUri(context, bitmap)
                    if (uri != null) {
                        onResult(uri)
                    }
                }
            }
        }
    )
    return rememberPermissionState(
        permission = Manifest.permission.CAMERA,
        onPermissionResult = { isGranted ->
            if (isGranted) {
                cameraLauncher.launch(null)
            } else {
                onPermissionDenied()
            }
        }
    )
}