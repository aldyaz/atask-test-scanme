@file:OptIn(ExperimentalPermissionsApi::class)

package com.aldyaz.scanme.main.component

import android.Manifest
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.Composable
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.PermissionState
import com.google.accompanist.permissions.rememberPermissionState

@Composable
fun rememberPictureInputState(
    onResult: (Uri) -> Unit,
    onPermissionDenied: () -> Unit
): PermissionState = rememberImagePickerPermissionState(
    onResult = onResult,
    onPermissionDenied = onPermissionDenied
)

@Composable
private fun rememberImagePickerPermissionState(
    onResult: (Uri) -> Unit,
    onPermissionDenied: () -> Unit
): PermissionState {
    val cameraLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent(),
        onResult = { uri ->
            if (uri != null) {
                onResult(uri)
            }
        }
    )
    return rememberPermissionState(
        permission = Manifest.permission.CAMERA,
        onPermissionResult = { isGranted ->
            if (isGranted) {
                cameraLauncher.launch("image/*")
            } else {
                onPermissionDenied()
            }
        }
    )
}